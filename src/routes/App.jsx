import { lazy, Suspense} from 'react'

import { Route, Routes, useNavigate } from 'react-router-dom'

// Layouts
import PrincipalLayout from '@/layouts/PrincipalLayout/PrincipalLayout'

// Loading Page
import Loading from '@/pages/Loading/Loading'

const Home = lazy(() => import('@/pages/Home/Home'))
const Users = lazy(() => import('@/pages/Users/Users'))
const NotFound = lazy(() => import('@/pages/NotFound/NotFound'))

export default function App() {

  return (
    <Suspense fallback={<Loading/>}>
      <Routes>
        <Route path='/' element={<PrincipalLayout /> }>
          <Route index element={<Home/>} />
          <Route path='home' element={<Home />} />
          <Route path='users' element={<Users />} />
        </Route>
        <Route path='*' element={<NotFound />} />
      </Routes>
    </Suspense>
  )
}

