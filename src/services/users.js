import {axiosInstance} from './instances';

const getUsers = async() => {
    const {data} = await axiosInstance.get('/users?limit=10');
    return data;
}

const getPositions = async() => {
    const {data} = await axiosInstance.get('/positions?limit=10');
    return data;
}

export {
    getUsers,
    getPositions
};