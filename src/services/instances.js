import axios from 'axios'

const API_BASE_URL = "https://mockend.com/LauraMamian/tbbc"

const axiosInstance = axios.create({
  baseURL: API_BASE_URL,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json'
  }
})

export {
  axiosInstance,
}