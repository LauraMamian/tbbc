import React from 'react'

import { useRecoilState } from 'recoil'
import { navbarState } from '@/atoms/navbarAtom'
import { Link } from 'react-router-dom'
import Logo from '../../assets/logo.png'
import Hamburger from 'hamburger-react'
import Navlinks from './Navlinks'

export default function Navbar() {
  const [{ isOpen }, setNavbar] = useRecoilState(navbarState)

  return (
    <div className='bg-slate-900 h-16 w-full flex justify-center fixed z-20'>
      <div className='flex h-full justify-between items-center w-11/12'>
        <Link to='/home' className='z-30'>
          <img src={Logo} className='w-12 h-auto' alt='Logo' />
        </Link>

        {/* Mobile Navigation */}
        <div className='md:hidden z-30'>
          <Hamburger
            rounded
            toggled={isOpen}
            toggle={() => setNavbar({ isOpen: !isOpen })}
            direction='left'
            duration={0.4}
            size={32}
          />
        </div>

        <div className='fixed w-full md:hidden'>
          {isOpen && <Navlinks />}
        </div>

        {/* Desktop Navigation */}
        <div className='hidden md:block h-full'>
          <Navlinks />
        </div>

      </div>
    </div>
  )
}
