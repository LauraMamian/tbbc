import { Link, useLocation } from 'react-router-dom'
import { HiHome } from 'react-icons/hi'
import { FaUser } from 'react-icons/fa'
import { useRecoilState } from 'recoil'
import { navbarState } from '@/atoms/navbarAtom'

export default function Navlinks() {
  // eslint-disable-next-line no-unused-vars
  const [navbar, setNavbar] = useRecoilState(navbarState)
  const location = useLocation()

  const activeLink = (path) => {
    const baseStyle = 'flex items-center h-full w-full rounded border-l-4 md:px-4 md:h-2/3 md:justify-center md:border-0 md:border-b-2 md:rounded-sm'
    if (location.pathname === path) {
      return (`${baseStyle} border-white text-white md:border-fuchsia-500 md:text-fuchsia-500`)
    } else {
      return (`${baseStyle} border-slate-500 text-slate-500 hover:text-white duration-200 md:hover:border-fuchsia-500 md:hover:text-fuchsia-500`)
    }
  }

  return (
    <nav className='animate__animated animate__slideInRight md:animate-none z-10 bg-slate-900 w-full pt-16 min-h-full fixed top-0 left-0 flex justify-center md:pt-0 md:flex md:justify-center md:items-center md:z-auto md:relative md:h-full'>
      <ul className='w-full md:flex md:h-full md:items-center'>
        <li className='h-14 flex items-center justify-center text-xl md:hidden md:mr-4'>
          Bienvenido
        </li>

        <li className='mb-2 h-14 md:flex md:mr-4 md:justify-center md:items-center md:h-full'>
          <Link
            to='/home'
            className={activeLink('/home')}
            onClick={() => setNavbar({ isOpen: false })}
          >
            <HiHome className='w-8 h-auto mx-4 md:hidden' />
            Inicio
          </Link>
        </li>

        <li className='mb-2 h-14 md:flex md:mr-4 md:justify-center md:items-center md:h-full'>
          <Link
            to='/users'
            className={activeLink('/users')}
            onClick={() => setNavbar({ isOpen: false })}
          >
            <FaUser className='w-8 h-auto mx-4 md:hidden' />
            Usuarios
          </Link>
        </li>
      </ul>
    </nav>
  )
}
