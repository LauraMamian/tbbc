import React from 'react'
import { FiGithub } from 'react-icons/fi'
import { FaLinkedin } from 'react-icons/fa'

export default function Footer() {
    return (
        <div className='flex justify-center items-center align-middle p-10'>
            <a href="https://github.com/LauraMamian">
                <FiGithub color='white' className='w-8 h-auto' />
            </a>
            <p className='mx-5'> Laura Andrea Mamián Cerón </p>
            <a href="https://www.linkedin.com/in/lauramamianc/">
                <FaLinkedin color='white' className='w-8 h-auto' />
            </a>
        </div>
    )
}
