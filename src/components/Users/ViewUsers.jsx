import { useEffect, useState } from 'react'
import { FaEdit, FaTrash, FaPlus } from 'react-icons/fa'
import { GoAlert } from 'react-icons/go'
import { useRecoilState } from 'recoil'

import {
  usersState,
  addUsersViewState,
  editUsersViewState,
  deleteUsersViewState
} from '@/atoms/usersAtom'

import {
  AddUser,
  EditUser,
  DeleteUser
} from '@/components/Users'

import { getUsers } from '@/services/users'

import LoadingSVG from '@/components/LoadingSVG/LoadingSVG'

export function ViewUsers() {
  const [addUsersView, setAddUsersView] = useRecoilState(addUsersViewState)
  const [editUsersView, setEditUsersView] = useRecoilState(editUsersViewState)
  const [deleteUsersView, setDeleteUsersView] = useRecoilState(deleteUsersViewState)
  const [users, setUsers] = useRecoilState(usersState)

  useEffect(() => {

    const localUsers = JSON.parse(localStorage.getItem('users')) || []

    getUsers()
      .then((data) => {
        setUsers([
          ...localUsers,
          ...data
        ])
      })
      .catch((err) => {
        setUsers(localUsers)
        console.log(err)
      })
  }, [])

  const tableHeaders = [
    'Nombre completo',
    'Correo electrónico',
    'Acción'
  ]

  const openAddView = () => {
    setAddUsersView({
      isOpen: true
    })
  }

  const openEditView = (user) => {
    setEditUsersView({
      isOpen: true,
      user
    })
  }

  const openDeleteView = (user) => {
    setDeleteUsersView({
      isOpen: true,
      user
    })
  }

  return (
    <>
      {
        addUsersView.isOpen && (<AddUser />)
      }
      {
        editUsersView.isOpen && (<EditUser />)
      }
      {
        deleteUsersView.isOpen && (<DeleteUser />)
      }

      <div className='flex flex-col justify-center items-center mb-4 md:flex-row'>
        <GoAlert className='w-8 h-auto' />
        <span className='md:ml-4'> Sólo puedes gestionar los usuarios creados por ti mismo. Al recargar la página, los datos de la API serán diferentes. </span>
      </div>

      <button
        onClick={openAddView}
        className='mb-4 bg-violet-500 flex flex-row justify-self-end items-center hover:bg-violet-600 duration-200 hover:duration-200 text-white p-2 w-fit rounded'
      >
        <FaPlus className='mr-2' />
        <span>
          Añadir usuario
        </span>
      </button>

      <table className='w-full mb-2 bg-slate-700 rounded-lg text-sm text-left text-slate-400'>
        <thead className='text-xs uppercase text-slate-400'>
          <tr>
            {
              tableHeaders.map((header, index) => (
                <th key={index} className={`py-3 pl-2 ${(header !== 'Nombre completo' && header !== 'Acción') && 'hidden md:block'}`}>
                  {header}
                </th>
              ))
            }
          </tr>
        </thead>

        {
          !users ? (
            <div className='w-full flex justify-center'>
              <LoadingSVG />
            </div>
          ) : (
            <tbody>
              {
                users.lenght !== 0 && users?.map((user, index) => (
                  <tr key={index} className='border-b bg-slate-800 border-slate-700'>
                    <th className='pl-2 px-6 text-ellipsis font-medium whitespace-nowrap text-white'>
                      {user.firstName} {user.lastName}
                    </th>

                    <td className='hidden md:table-cell pl-2 text-ellipsis px-6 font-medium whitespace-nowrap text-white'>
                      {user.email}
                    </td>

                    <td className='py-2 pl-2 grid w-fit grid-cols-2 gap-2'>
                      <button
                        onClick={() => openEditView(user)}
                        className='bg-violet-500 hover:bg-violet-600 duration-200 hover:duration-200 text-white p-2 w-fit rounded'
                      >
                        <FaEdit />
                      </button>
                      <button
                        onClick={() => openDeleteView(user)}
                        className='bg-red-500 hover:bg-red-600 duration-200 hover:duration-200 text-white p-2 w-fit rounded'
                      >
                        <FaTrash />
                      </button>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          )
        }

        <tfoot className='text-xs uppercase text-slate-400'>
          <tr>
            {
              tableHeaders.map((header, index) => (
                <th key={index} scope='col' className={`py-3 pl-2 ${(header !== 'Nombre completo' && header !== 'Acción') ? 'hidden md:block' : ''}`}>
                  {header}
                </th>
              ))
            }
          </tr>
        </tfoot>
      </table>



    </>

  )
}
