import React from 'react'

import { FiAlertTriangle } from 'react-icons/fi'
import { CgTrash } from 'react-icons/cg'

import { useRecoilState } from 'recoil'
import { deleteUsersViewState, usersState } from '@/atoms/usersAtom'

export function DeleteUser() {
  const [{ user }, setDeleteUsersView] = useRecoilState(deleteUsersViewState)
  const [users, setUsers] = useRecoilState(usersState)

  const closeDeleteView = () => {
    setDeleteUsersView({
      isOpen: false,
      user: null
    })
  }

  const handleDelete = () => {
    console.clear()
    console.log('User Deleted')
    console.table(user)

    const newUsers = users.filter(u => u.id !== user.id)
    setUsers(newUsers)

    const prevUsers = JSON.parse(localStorage.getItem('users'))
    localStorage.setItem('users', JSON.stringify(prevUsers.filter(u => u.id !== user.id)))

    closeDeleteView()
  }

  return (
    <div className='fixed z-30 top-0 left-0 h-full min-h-screen w-full'>
      <div className='relative h-full w-full'>
        <div className='absolute h-full w-full grid place-items-center top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2'>
          <div onClick={closeDeleteView} className='absolute h-full w-full bg-slate-700 opacity-20 ' />
          <div className='max-h-[90vh] animate__animated animate__fadeInDown animate__faster bg-slate-900 shadow overflow-y-auto pb-6 px-6 w-11/12 md:w-5/6 xl:w-2/3 flex flex-col justify-start rounded-lg z-10'>
            <h3 className='text-white mt-10 text-2xl text-center mb-4 w-full'>
              Eliminar usuario
            </h3>
            <div className='my-3'>

              <FiAlertTriangle className='text-7xl text-primary mx-auto' />
              <h3 className='text-primary text-2xl text-center mb-4 w-full'>
                Are you sure you want to delete {user.firstName}?
              </h3>
              <div className='grid gap-5 grid-cols-2'>
                <button onClick={() => handleDelete()} className='rounded text-md py-2 border duration-200 hover:duration-200 hover:bg-red-600 hover:border-red-600 border-red-500 bg-red-500 text-white flex flex-rows items-center justify-center'>
                  <CgTrash className='text-lg mr-2' />
                  Yes, Delete it
                </button>
                <button onClick={() => closeDeleteView()} className='rounded py-2 border border-violet-500 duration-200 hover:duration-200 hover:bg-violet-600 hover:border-violet-600 hover:text-white bg-violet-500 text-white'>
                  No, Cancel
                </button>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
