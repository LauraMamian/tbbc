import React, { useEffect, useState } from 'react'

import { useRecoilState } from 'recoil'
import {
  addUsersViewState,
  usersState,
  positionsState
} from '@/atoms/usersAtom'

import { getPositions } from '@/services/users'

import { useForm } from 'react-hook-form'

export function AddUser() {

  const [{ isOpen }, setAddUsersView] = useRecoilState(addUsersViewState)
  const [users, setUsers] = useRecoilState(usersState)
  const [positions, setPositions] = useRecoilState(positionsState)
  const [form, setForm] = useState(null)

  const { handleSubmit, register, formState: { errors } } = useForm()

  useEffect(() => {
    getPositions()
      .then((data) => {
        setPositions(data)
      })
      .catch((err) => {
        console.log(err)
      })
  }, [])

  const onSubmit = (newUser) => {
    console.clear()
    console.log('User Added')

    const newUserWithId = {
      id: Date.now(),
      ...newUser
    }

    const prevUsers = JSON.parse(localStorage.getItem('users')) || []
    const newUsers = [newUserWithId, ...prevUsers]
    localStorage.setItem('users', JSON.stringify(newUsers))

    setUsers([newUserWithId, ...users])
    console.log([newUserWithId, ...users])

    closeAddView()
  }

  const closeAddView = () => {
    setAddUsersView({
      isOpen: false
    })
  }

  useEffect(() => {
    if (positions !== null) {
      setForm({
        firstName: {
          id: 'firstName',
          name: 'firstName',
          label: 'Nombre',
          type: 'text',
          defaultValue: '',
          placeholder: ' ', // ! Important to animation
          validations: {
            required: {
              value: true,
              message: 'Campo obligatorio'
            },
            minLength: 2,
            maxLength: 30
          }
        },
        lastName: {
          id: 'lastName',
          name: 'lastName',
          label: 'Apellido',
          type: 'text',
          defaultValue: '',
          placeholder: ' ', // ! Important to animation
          validations: {
            required: {
              value: true,
              message: 'Campo obligatorio'
            },
            minLength: 2,
            maxLength: 30
          }
        },
        phone: {
          id: 'phone',
          name: 'phone',
          label: 'Teléfono',
          type: 'text',
          defaultValue: '',
          placeholder: ' ', // ! Important to animation
          validations: {
            required: {
              value: true,
              message: 'Campo obligatorio'
            },
            pattern: {
              value: /^[0-9]{10}$/,
              message: 'No es un número de teléfono válido'
            }
          }
        },
        ID: {
          id: 'ID',
          name: 'ID',
          label: 'Cédula de ciudadanía',
          type: 'text',
          defaultValue: '',
          placeholder: ' ', // ! Important to animation
          validations: {
            required: {
              value: true,
              message: 'Campo obligatorio'
            },
            pattern: {
              value: /^[0-9]{10}$/,
              message: 'No es un número de cédula válido'
            }
          }
        },
        email: {
          id: 'email',
          name: 'email',
          label: 'Correo electrónico',
          type: 'text',
          defaultValue: '',
          placeholder: ' ', // ! Important to animation
          validations: {
            required: {
              value: true,
              message: 'Campo obligatorio'
            },
            minLength: 2,
            maxLength: 30,
          }
        },
        address: {
          id: 'address',
          name: 'address',
          label: 'Dirección',
          type: 'text',
          defaultValue: '',
          placeholder: ' ', // ! Important to animation
          validations: {
            required: {
              value: true,
              message: 'Campo obligatorio'
            },
          }
        },
        dateBirth: {
          id: 'dateBirth',
          name: 'dateBirth',
          label: 'Fecha de nacimiento',
          type: 'date',
          defaultValue: '',
          placeholder: ' ', // ! Important to animation
          validations: {
            required: {
              value: true,
              message: 'Campo obligatorio'
            },
          }
        },
        position: {
          name: 'position',
          label: 'Selecione un cargo',
          defaultValue: '',
          options: [
            ...positions.map((p) => ({ value: p.id, label: p.position }))
          ]
        },
      })
    }
  }, [positions])

  return (
    <div className='fixed z-30 top-0 left-0 h-full min-h-screen w-full'>
      <div className='relative h-full w-full'>
        <div className='absolute h-full w-full grid place-items-center top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2'>
          <div onClick={closeAddView} className='absolute h-full w-full bg-slate-700 opacity-20 ' />
          <div className='max-h-[90vh] animate__animated animate__fadeInDown animate__faster bg-slate-900 shadow overflow-y-auto pb-6 px-6 w-11/12 md:w-5/6 xl:w-2/3 flex flex-col justify-start rounded-lg z-10'>
            <h3 className='text-white mt-10 text-2xl text-center mb-4 w-full'>
              Añadir usuario
            </h3>
            <div className='my-3'>

              {
                form !== null && (
                  <form onSubmit={handleSubmit(onSubmit)} className='grid grid-cols-1 gap-2 md:grid-cols-2'>

                    {/* Nombre  */}
                    <div>
                      <div className='relative w-full mb-2'>
                        <input
                          {...form.firstName}
                          {...register(form.firstName.name, { ...form.firstName.validations })}
                          className={`block h-12 px-[8px] w-full text-sm text-white bg-transparent ring-0 outline-none rounded-lg border border-slate-text-slate-200 appearance-none duration-200 focus:outline-none focus:ring-0 focus:duration-200 focus:border-violet-500 peer ${errors.firstName && 'border-red-400 focus:border-red-400'}`}
                        />
                        <label
                          htmlFor={form.firstName.name}
                          className={`absolute cursor-text text-sm text-slate-200 duration-300 transform -translate-y-4 scale-75 top-2 z-0 origin-[0] bg-slate-900 rounded-full px-2 peer-focus:px-2 peer-focus:text-violet-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-[6px] peer-focus:scale-75 peer-focus:-translate-y-4 left-1 ${errors.firstName && 'text-red-400 peer-focus:text-red-400'}`}
                        >
                          {form.firstName.label}
                        </label>
                      </div>
                      {errors.firstName && <span role='alert' className='block mb-4 border-l-4 border-l-red-400 pl-2 rounded bg-red-200 text-red-800'>{errors.firstName.message}</span>}
                    </div>

                    {/* Apellido */}
                    <div>
                      <div className='relative w-full mb-2'>
                        <input
                          {...form.lastName}
                          {...register(form.lastName.name, { ...form.lastName.validations })}
                          className={`block h-12 px-[8px] w-full text-sm text-white bg-transparent ring-0 outline-none rounded-lg border border-slate-text-slate-200 appearance-none duration-200 focus:outline-none focus:ring-0 focus:duration-200 focus:border-violet-500 peer ${errors.lastName && 'border-red-400 focus:border-red-400'}`}
                        />
                        <label
                          htmlFor={form.lastName.name}
                          className={`absolute cursor-text text-sm text-slate-200 duration-300 transform -translate-y-4 scale-75 top-2 z-0 origin-[0] bg-slate-900 rounded-full px-2 peer-focus:px-2 peer-focus:text-violet-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-[6px] peer-focus:scale-75 peer-focus:-translate-y-4 left-1 ${errors.lastName && 'text-red-400 peer-focus:text-red-400'}`}
                        >
                          {form.lastName.label}
                        </label>
                      </div>
                      {errors.lastName && <span role='alert' className='block mb-4 border-l-4 border-l-red-400 pl-2 rounded bg-red-200 text-red-800'>{errors.lastName.message}</span>}
                    </div>

                    {/* Teléfono */}
                    <div>
                      <div className='relative w-full mb-2'>
                        <input
                          {...form.phone}
                          {...register(form.phone.name, { ...form.phone.validations })}
                          className={`block h-12 px-[8px] w-full text-sm text-white bg-transparent ring-0 outline-none rounded-lg border border-slate-text-slate-200 appearance-none duration-200 focus:outline-none focus:ring-0 focus:duration-200 focus:border-violet-500 peer ${errors.phone && 'border-red-400 focus:border-red-400'}`}
                        />
                        <label
                          htmlFor={form.phone.name}
                          className={`absolute cursor-text text-sm text-slate-200 duration-300 transform -translate-y-4 scale-75 top-2 z-0 origin-[0] bg-slate-900 rounded-full px-2 peer-focus:px-2 peer-focus:text-violet-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-[6px] peer-focus:scale-75 peer-focus:-translate-y-4 left-1 ${errors.phone && 'text-red-400 peer-focus:text-red-400'}`}
                        >
                          {form.phone.label}
                        </label>
                      </div>
                      {errors.phone && <span role='alert' className='block mb-4 border-l-4 border-l-red-400 pl-2 rounded bg-red-200 text-red-800'>{errors.phone.message}</span>}
                    </div>

                    {/* Número de cédula */}
                    <div>
                      <div className='relative w-full mb-2'>
                        <input
                          {...form.ID}
                          {...register(form.ID.name, { ...form.ID.validations })}
                          className={`block h-12 px-[8px] w-full text-sm text-white bg-transparent ring-0 outline-none rounded-lg border border-slate-text-slate-200 appearance-none duration-200 focus:outline-none focus:ring-0 focus:duration-200 focus:border-violet-500 peer ${errors.ID && 'border-red-400 focus:border-red-400'}`}
                        />
                        <label
                          htmlFor={form.ID.name}
                          className={`absolute cursor-text text-sm text-slate-200 duration-300 transform -translate-y-4 scale-75 top-2 z-0 origin-[0] bg-slate-900 rounded-full px-2 peer-focus:px-2 peer-focus:text-violet-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-[6px] peer-focus:scale-75 peer-focus:-translate-y-4 left-1 ${errors.phone && 'text-red-400 peer-focus:text-red-400'}`}
                        >
                          {form.ID.label}
                        </label>
                      </div>
                      {errors.ID && <span role='alert' className='block mb-4 border-l-4 border-l-red-400 pl-2 rounded bg-red-200 text-red-800'>{errors.ID.message}</span>}
                    </div>

                    {/* Correo electrónico */}
                    <div>
                      <div className='relative w-full mb-2'>
                        <input
                          {...form.email}
                          {...register(form.email.name, { ...form.email.validations })}
                          className={`block h-12 px-[8px] w-full text-sm text-white bg-transparent ring-0 outline-none rounded-lg border border-slate-text-slate-200 appearance-none duration-200 focus:outline-none focus:ring-0 focus:duration-200 focus:border-violet-500 peer ${errors.email && 'border-red-400 focus:border-red-400'}`}
                        />
                        <label
                          htmlFor={form.email.name}
                          className={`absolute cursor-text text-sm text-slate-200 duration-300 transform -translate-y-4 scale-75 top-2 z-0 origin-[0] bg-slate-900 rounded-full px-2 peer-focus:px-2 peer-focus:text-violet-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-[6px] peer-focus:scale-75 peer-focus:-translate-y-4 left-1 ${errors.email && 'text-red-400 peer-focus:text-red-400'}`}
                        >
                          {form.email.label}
                        </label>
                      </div>
                      {errors.email && <span role='alert' className='block mb-4 border-l-4 border-l-red-400 pl-2 rounded bg-red-200 text-red-800'>{errors.email.message}</span>}
                    </div>

                    {/* Dirección */}
                    <div>
                      <div className='relative w-full mb-2'>
                        <input
                          {...form.address}
                          {...register(form.address.name, { ...form.address.validations })}
                          className={`block h-12 px-[8px] w-full text-sm text-white bg-transparent ring-0 outline-none rounded-lg border border-slate-text-slate-200 appearance-none duration-200 focus:outline-none focus:ring-0 focus:duration-200 focus:border-violet-500 peer ${errors.address && 'border-red-400 focus:border-red-400'}`}
                        />
                        <label
                          htmlFor={form.address.name}
                          className={`absolute cursor-text text-sm text-slate-200 duration-300 transform -translate-y-4 scale-75 top-2 z-0 origin-[0] bg-slate-900 rounded-full px-2 peer-focus:px-2 peer-focus:text-violet-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-[6px] peer-focus:scale-75 peer-focus:-translate-y-4 left-1 ${errors.address && 'text-red-400 peer-focus:text-red-400'}`}
                        >
                          {form.address.label}
                        </label>
                      </div>
                      {errors.address && <span role='alert' className='block mb-4 border-l-4 border-l-red-400 pl-2 rounded bg-red-200 text-red-800'>{errors.address.message}</span>}
                    </div>

                    {/* Fecha de nacimiento  */}
                    <div>
                      <div className='relative w-full mb-2'>
                        <input
                          {...form.dateBirth}
                          {...register(form.dateBirth.name, { ...form.dateBirth.validations })}
                          className={`block h-12 px-[8px] w-full text-sm text-white bg-transparent ring-0 outline-none rounded-lg border border-slate-text-slate-200 appearance-none duration-200 focus:outline-none focus:ring-0 focus:duration-200 focus:border-violet-500 peer ${errors.dateBirth && 'border-red-400 focus:border-red-400'}`}
                        />
                        <label
                          htmlFor={form.dateBirth.name}
                          className={`absolute cursor-text text-sm text-slate-200 duration-300 transform -translate-y-4 scale-75 top-2 z-0 origin-[0] bg-slate-900 rounded-full px-2 peer-focus:px-2 peer-focus:text-violet-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-[6px] peer-focus:scale-75 peer-focus:-translate-y-4 left-1 ${errors.dateBirth && 'text-red-400 peer-focus:text-red-400'}`}
                        >
                          {form.dateBirth.label}
                        </label>
                      </div>
                      {errors.dateBirth && <span role='alert' className='block mb-4 border-l-4 border-l-red-400 pl-2 rounded bg-red-200 text-red-800'>{errors.dateBirth.message}</span>}
                    </div>

                    {/* Cargo */}
                    <div className='relative w-full mb-2'>
                      <select
                        {...form.position}
                        {...register(form.position.name, { ...form.position.validations })}
                        className={`block h-12 px-[8px] w-full text-sm text-secondary bg-transparent ring-0 outline-none rounded-lg border border-slate-text-slate-200 appearance-none duration-200 focus:outline-none focus:ring-0 focus:duration-200 focus:border-violet-500 peer ${errors.position && 'border-red-400 focus:border-red-400'}`}
                      >
                        {form.position.options.map((option, index) => (
                          <option className='bg-slate-900' key={index} value={option.value}>{option.label}</option>
                        ))}
                      </select>

                      {form.position.label && (
                        <label
                          htmlFor={form.position.name}
                          className={`absolute cursor-text text-sm text-slate-200 duration-300 transform -translate-y-4 scale-75 top-2 z-0 origin-[0] bg-slate-900 rounded-full px-2 peer-focus:px-2 peer-focus:text-violet-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-[6px] peer-focus:scale-75 peer-focus:-translate-y-4 left-1 ${errors.dateBirth && 'text-red-400 peer-focus:text-red-400'}`}
                        >
                          {form.position.label}
                        </label>
                      )}
                    </div>

                    <button
                      className='bg-violet-500 md:col-span-2 items-center h-12 hover:bg-violet-600 duration-200 hover:duration-200 text-white px-4 py-2 rounded'
                      type='submit'
                    >
                      Submit
                    </button>

                  </form>
                )
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
