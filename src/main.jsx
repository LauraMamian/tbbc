import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter as Router } from 'react-router-dom'

import App from '@/routes/App'

import { RecoilRoot } from 'recoil'

import './index.css'
import 'animate.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <Router>
    <RecoilRoot>
      <App />
    </RecoilRoot>
  </Router>
)
