import React from 'react'
import { Outlet } from 'react-router-dom'
import Footer from '@/components/Footer/Footer'
import Navbar from '@/components/Navbar/Navbar'

export default function PrincipalLayout () {
  return (
    <div className='flex flex-col bg-slate-900 text-white min-h-screen justify-between items-center'>
      <Navbar />
      <div className='w-11/12 md:w-3/4 mt-20 grid place-items-center'>
        <Outlet />
      </div>
      <Footer />
    </div>
  )
}
