import { atom } from 'recoil'

export const navbarState = atom({
  key: 'navbar',
  default: {
    isOpen: false
  }
})
