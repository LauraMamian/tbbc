import { atom } from 'recoil'

export const editUsersViewState = atom({
  key: 'editUsersView',
  default: {
    isOpen: false,
    user: null
  }
})

export const addUsersViewState = atom({
  key: 'addUsersView',
  default: {
    isOpen: false
  }
})

export const deleteUsersViewState = atom({
  key: 'deleteUsersView',
  default: {
    isOpen: false,
    user: null
  }
})

export const usersState = atom({
  key: 'users',
  default: null
})

export const positionsState = atom({
  key: 'positions',
  default: null
})