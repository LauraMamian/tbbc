import React from 'react'
import ImgUsers from '../../assets/users.svg'

export default function Home() {
  return (
    <div className='w-11/12 max-h-screen flex flex-col justify-center items-center md:justify-start'>
      <div className='flex flex-col items-center justify-center md:items-center md:w-full'>

        <h1 className='animate__animated animate__fadeInDown text-4xl md:text-4xl font-bold'> Bienvenido </h1>

        <img className='animate__animated animate__fadeInDown w-1/4 my-8 md:justify-center' src={ImgUsers} alt='ImgUsers' />

        <p className='animate__animated animate__fadeInDown text-slate-500 md:mb-4 text-center md:text-left'>
          Este es un sistema de gestión de usuarios usando {' '}
          <span className='text-fuchsia-500'> Fake API </span>.
        </p>

        <p className='w-1/2 animate__animated animate__fadeInDown text-slate-500 md:mb-4 text-center md:text-center'>
          Gracias por la oportunidad de permitirme presentar esta prueba técnica, con la construcción de este proyecto aumentaron mis conocimientos en React 🙋‍♀️
        </p>

      </div>
    </div>
  )
}