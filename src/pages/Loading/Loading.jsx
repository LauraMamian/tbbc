import React from 'react'

import LoadingSVG from '@/components/LoadingSVG/LoadingSVG'

export default function Loading () {
  return (
    <div className='w-full min-h-screen flex items-center justify-center bg-slate-900'>
      <LoadingSVG />
    </div>
  )
}