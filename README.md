# Prueba Técnica React I


## Reto

Crear una aplicación React que permita gestionar (CRUD) los perfiles de los empleados en una empresa.

1. La interfaz puede tener el diseño que el desarrollador considere más apropiado.
    - Debe componerse de un formulario con los siguientes campos:
        - Nombres.
        - Apellidos.
        - Teléfono.
        - Número de cédula.
        - Correo.
        - Dirección.
        - Fecha de nacimiento.
        - Cargo.

    - Lista de los empleados creados (Esta debe permitir organizar a los empleados por Nombre, editar y eliminar cada uno de estos).

    - Tener en cuenta restricciones y validaciones de los campos.

2. El código debe postearse en GITLAB junto con las instrucciones para su configuración y ejecución en un entorno de desarrollo.

3. El frontend debe conectarse con un backend, el desarrollador puede utilizar un servicio mock de APIS o crear su propia API.

## 🚀 Ejecución del proyecto

Se utilizará este repositorio y para ello debe seguir los siguientes pasos:

```
git clone https://gitlab.com/LauraMamian/tbbc.git
cd tbbc
npm i
npm run dev
```

_*_Consideraciones: Algunas de las librerías podrían no ser instaladas con el comando ```npm i```. Es necesario que se instalen manualmente._*_

## 📝 Descripción del proyecto

Este proyecto está compuesto por dos páginas, una de "Inicio" donde se muestra un breve saludo a quien interactúa con la aplicación y una página "Usuarios", donde se podrá verificar el funcionamiento de la gestión de usuarios.

<p align="center">
    <img
        src="src/assets/images/1.png"
        width=100%
    >
</p>

<p align="center">
    <img
        src="src/assets/images/2.png"
        width=100%
    >
</p>

En esta página se pueden ver los usuarios registrados desde la API de Mockend, además, se encuentran los botones para añadir, modificar o eliminar usuarios. Según la documentación de Mockend, las peticiones ``POST, PUT, PATCH y DELETE`` se simulan y los cambios no se guardan, por lo que es necesario gestionar los usuarios de manera local, es decir, añadiendo un nuevo usuario para posteriormente modificarlo o eliminarlo.

Para más información, consulte la documentación oficial de Mockend:

[Mockend](https://docs.mockend.com/)

## 💻 Requisitos

- Instalar Node.js (versión 16 o superior).

## 🎈 Colaboradores

Hecho por [@LauraMamian](https://www.github.com/LauraMamian).