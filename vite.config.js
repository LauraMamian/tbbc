import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@/components': path.resolve(__dirname, './src/components/'),
      '@/atoms': path.resolve(__dirname, './src/atoms/'),
      '@/layouts': path.resolve(__dirname, './src/layouts/'),
      '@/hooks': path.resolve(__dirname, './src/hooks/'),
      '@/routes': path.resolve(__dirname, './src/routes/'),
      '@/services': path.resolve(__dirname, './src/services/'),
      '@/pages': path.resolve(__dirname, './src/pages/'),
    }
  },
  plugins: [react()],
})
